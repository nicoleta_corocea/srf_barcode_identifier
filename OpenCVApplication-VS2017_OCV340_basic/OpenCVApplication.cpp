// OpenCVApplication.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "common.h"
#include <iostream>
#include <fstream>
#include <string>

RNG rng(12345);
std::vector<std::string> firstG{ "LLLLLL","LLGLGG","LLGGLG","LLGGGL","LGLLGG","LGGLLG","LGGGLL","LGLGLG","LGLGGL","LGGLGL" };
std::vector<std::string> lDigit{ "0001101","0011001","0010011","0111101","0100011","0110001","0101111","0111011","0110111","0001011" };
std::vector<std::string> rDigit{ "1110010","1100110","1101100","1000010","1011100","1001110","1010000","1000100","1001000","1110100" };
std::vector<std::string> gDigit{ "0100111","0110011","0011011","0100001","0011101","0111001","0000101","0010001","0001001","0010111" };
Mat processCanny(Mat src)
{
	Mat dst, gauss;
	double k = 0.4;
	int pH = 50;
	int pL = (int)k*pH;
	GaussianBlur(src, gauss, Size(5, 5), 0.8, 0.8);
	Canny(gauss, dst, pL, pH, 3);
	return dst;

}

Mat rotate(Mat src, double angle)
{
	Mat dst;
	Point2f pt(src.cols / 2., src.rows / 2.);
	Mat r = getRotationMatrix2D(pt, angle, 1.0);
	warpAffine(src, dst, r, Size(src.cols, src.rows));
	return dst;
}


Mat get_barcode_image(Mat src) {
	Mat img;
	cvtColor(src, img, cv::COLOR_RGB2GRAY);
	int scale = 1;
	int delta = 0;
	int ddepth = CV_32F;
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;
	Sobel(img, grad_x, ddepth, 1, 0, -1);
	Sobel(img, grad_y, ddepth, 0, 1, -1);
	Mat gradient;
	convertScaleAbs(grad_x - grad_y, gradient);
	GaussianBlur(src, src, Size(3, 3), 0, 0, BORDER_DEFAULT);

	Mat thresh;
	threshold(gradient, thresh, 225, 255, THRESH_BINARY);

	Mat kernel = getStructuringElement(MORPH_RECT, Size(10, 7));
	Mat closed;
	morphologyEx(thresh, closed, MORPH_CLOSE, kernel);
	Mat element = getStructuringElement(0, Size(3, 3), Point(1, 1));

	erode(closed, closed, element, Point(-1, -1), 4);
	dilate(closed, closed, element, Point(-1, -1), 4);
	imshow("closed", closed);
	std::vector<std::vector<Point>> contours;
	std::vector<Vec4i> hierarchy;

	findContours(closed, contours, hierarchy,
		CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

	std::vector<std::vector<Point> > contours_poly(contours.size());
	std::vector<Rect> boundRect(contours.size());
	std::vector<Point2f>center(contours.size());
	std::vector<float>radius(contours.size());

	for (int i = 0; i < contours.size(); i++)
	{
		approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
	}


	//detect the largest 
	Point2i tl = boundRect[0].tl();
	Point2i br = boundRect[0].br();
	int maxArea = (boundRect[0].br().x - boundRect[0].tl().x)*(boundRect[0].br().y - boundRect[0].tl().y);
	for (int i = 1; i < contours.size(); i++) {
		if ((boundRect[i].br().x - boundRect[i].tl().x)*(boundRect[i].br().y - boundRect[i].tl().y) > maxArea) {
			maxArea = (boundRect[i].br().x - boundRect[i].tl().x)*(boundRect[i].br().y - boundRect[i].tl().y);
			tl = boundRect[i].tl();
			br = boundRect[i].br();
		}
	}

	Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
	rectangle(src, tl, br, color, 2, 8, 0);
	imshow("rectangle select", src);
	//select barcode
	Mat barcode(img, Rect(tl.x, tl.y, abs(br.x - tl.x), abs(br.y - tl.y)));
	/*for (int i = 0; i < barcode.rows; i++) {
		for (int j = 0; j < barcode.cols; j ++ ) {
			if (barcode.at<uchar>(i, j) > 100) {
				barcode.at<uchar>(i, j) = 255;
			}
			else
			{
				barcode.at<uchar>(i, j) = 0;
			}
		}
	}*/
	return barcode;

}


void barcode_identifier() {
	char fname[MAX_PATH];
	Mat dt, img;
	int nrLines = 3;
	if (openFileDlg(fname)) //invoca fereastra de afisare pt a putea sa alegem o imagine
							//are proprietatea de a popula path-ul
	{
		img = imread(fname, CV_LOAD_IMAGE_COLOR);
	}
	//Mat th = thresholding(src);
	Mat src;
	cvtColor(img, src, cv::COLOR_RGB2GRAY);
	Mat edges = processCanny(src);

	std::vector<Vec2f> lines; // will hold the results of the detection
	HoughLines(edges, lines, 1, CV_PI / 180, nrLines, 0, 0); // runs the actual detection
	Mat cdst;
	cvtColor(edges, cdst, COLOR_GRAY2BGR);
	double meanThetaAngle = 0;
	for (int i = 0; i < nrLines; i++) {
		float rho = lines[i][0], theta = lines[i][1];
		meanThetaAngle += theta * 180 / PI;
		printf("%f %f\n", rho, theta);
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a * rho, y0 = b * rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, LINE_AA);
	}


	meanThetaAngle /= nrLines;
	printf("mean theta %f", meanThetaAngle);
	Mat rotated;
	if (meanThetaAngle > 90)
	{
		rotated = rotate(img, meanThetaAngle - 180);
	}
	else
	{
		rotated = rotate(img, meanThetaAngle);
	}

	imshow("rotated ", rotated);
	Mat gradient = get_barcode_image(rotated);

	std::vector<uchar> row;
	gradient.row(gradient.rows / 2).copyTo(row);

	imshow("barcode image", gradient);

	waitKey();
}
int main()
{
	int op;
	do
	{
		system("cls");
		destroyAllWindows();
		printf("Menu:\n");
		printf(" 1 - barcode\n");
		printf(" 0 - Exit\n\n");
		printf("Option: ");
		scanf("%d", &op);
		switch (op)
		{
		case 1: barcode_identifier();
			break;
		}
	} while (op != 0);
	return 0;
}